/************************************************************************
*  This file is part of the JUMMP Importer Project.
*    Copyright (C) 2011  Martin Gräßlin <m.graesslin@dkfz.de>
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/
#include <QCoreApplication>
#include "jummp-loader.h"
#include <QStringList>
#include <iostream>

int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);
    QStringList arguments = app.arguments();
    if (arguments.size() != 2 && arguments.size() != 3 && arguments.size() != 4) {
        std::cerr << "You have to specifiy the directory of the Model files as a parameter!" << std::endl;
        std::cerr << "Exiting...." << std::endl;
        return 1;
    }
    bool publish = false;
    bool sessionBus = false;
    for (int i=1; i<arguments.size()-1; ++i) {
        if (arguments[i] == "--publish") {
            publish = true;
            continue;
        }
        if (arguments[i] == "--session-bus") {
            sessionBus = true;
            continue;
        }
        if (arguments[i] == "--system-bus") {
            sessionBus = false;
            continue;
        }
        // not one of our parameters
        std::cerr << "Unknown parameter: " << arguments[i].toUtf8().constData() << std::endl;
        std::cerr << "Exiting...." << std::endl;
        return 1;
    }
    JummpLoader jummpLoader(arguments.last(), publish, sessionBus);
    QCoreApplication::connect(&jummpLoader, SIGNAL(quit()), &app, SLOT(quit()));
    return app.exec();
}
