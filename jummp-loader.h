/************************************************************************
*  This file is part of the JUMMP Importer Project.
*    Copyright (C) 2011  Martin Gräßlin <m.graesslin@dkfz.de>
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/
#ifndef jummp_loader_H
#define jummp_loader_H

#include <QtCore/QObject>
#include <QtCore/QDir>
#include <QtCore/QMap>
#include <QtXml/QXmlStreamReader>

class QDBusInterface;
class QFile;
class QTimer;

class JummpLoader : public QObject
{
Q_OBJECT
public:
    JummpLoader(const QString &directory, bool publish, bool sessionBus);
    virtual ~JummpLoader();

signals:
    void quit();

public slots:
    /**
     * Slot to start the processing of all Model files.
     **/
    void process();
    void feedback();
    void handleProcessingFinished();

private:
    /**
     * Performs the initialization by creating the DBusInterface and authenticating the user.
     * @returns @c false if an error occurred, @c true otherwise.
     **/
    bool init();
    /**
     * Tries to authenticate the user and returns the authentication hash. If the authentication
     * fails an empty String is returned.
     * @returns The authentication hash.
     **/
    bool authenticate();

    /**
     * Imports the Model with file @p path with @p name.
     *
     * @param path The absolute path to the Model file.
     * @param name The name of the Model.
     **/
    void importModel(const QString &path, const QString &name);
    /**
     * Prints some summary information about the import process.
     **/
    void printResults();
    void performImport();
    void performFileImport(const QFileInfo &info);
    QString extractName(QFile *file);

    QDir m_dir;
    QString m_authenticationHash;
    QDBusInterface *m_modelInterface;
    QMap<QString, QString> m_errors;
    QTimer *m_feedbackTimer;
    QXmlStreamReader m_xml;
    bool m_publish;
    bool m_sessionBus;
};

#endif // jummp-loader_H
