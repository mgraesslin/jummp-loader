/************************************************************************
*  This file is part of the JUMMP Importer Project.
*    Copyright (C) 2011  Martin Gräßlin <m.graesslin@dkfz.de>
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/
#include "jummp-loader.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QtConcurrentRun>
#include <QtCore/QDebug>
#include <QtCore/QFileInfo>
#include <QtCore/QFuture>
#include <QtCore/QFutureWatcher>
#include <QtCore/QStringList>
#include <QtCore/QTimer>
#include <QtDBus/QDBusInterface>
#include <QtDBus/QDBusReply>
#include <iostream>
#include <termios.h>
#include <unistd.h>

JummpLoader::JummpLoader(const QString &directory, bool publish, bool sessionBus)
    : QObject()
    , m_dir(QDir(directory))
    , m_publish(publish)
    , m_sessionBus(sessionBus)
{
    if (!m_dir.exists()) {
        QTimer::singleShot(0, this, SIGNAL(quit()));
        return;
    }
    m_dir.setSorting(QDir::Name);
    m_feedbackTimer = new QTimer(this);
    connect(m_feedbackTimer, SIGNAL(timeout()), SLOT(feedback()));
    QTimer::singleShot(0, this, SLOT(process()));
}

JummpLoader::~JummpLoader()
{}

bool JummpLoader::init()
{
    m_modelInterface = new QDBusInterface("net.biomodels.jummp", "/Model", "net.biomodels.jummp.Model", m_sessionBus ? QDBusConnection::sessionBus() : QDBusConnection::systemBus(), this);
    if (!m_modelInterface->isValid()) {
        std::cerr << "Model DBus Interface not valid" << std::endl;
        return false;
    }
    if (!authenticate()) {
        return false;
    }
    return true;
}

void JummpLoader::process()
{
    if (!init()) {
        emit quit();
        return;
    }
    // starting as a thread
    std::cout << "Starting Processing all files" << std::endl;
    m_feedbackTimer->start(100);

    QFutureWatcher< void > *watcher = new QFutureWatcher<void>(this);
    connect(watcher, SIGNAL(finished()), SLOT(handleProcessingFinished()));
    QFuture<void> future = QtConcurrent::run(this, &JummpLoader::performImport);
    watcher->setFuture(future);
}

bool JummpLoader::authenticate()
{
    // read in username and password
    std::cout << "JUMMP username:";
    std::string username;
    std::string password;
    std::getline(std::cin, username);

    struct termios currentSettings, newSettings;
    tcgetattr(STDIN_FILENO, &currentSettings);
    newSettings = currentSettings;
    newSettings.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newSettings);
    std::cout << "Password:";
    std::getline(std::cin, password);
    tcsetattr(STDIN_FILENO, TCSANOW, &currentSettings);

    // try to authenticate
    QDBusInterface appInterface("net.biomodels.jummp", "/Application", "net.biomodels.jummp.application", m_sessionBus ? QDBusConnection::sessionBus() : QDBusConnection::systemBus());
    QDBusMessage message = appInterface.call("authenticate", QString::fromStdString(username), QString::fromStdString(password));
    if (message.type() == QDBusMessage::ErrorMessage) {
        std::cerr << "Authentication failed: " << appInterface.lastError().message().toUtf8().constData() << std::endl;
        return false;
    }

    m_authenticationHash = message.arguments().at(1).toString();
    return true;
}

void JummpLoader::importModel(const QString &path, const QString &name)
{

    QList<QVariant> arguments;
    //method qlonglong net.biomodels.jummp.model.uploadModel(QString, QString, qlonglong, QString, QString, QString, QString, QString, qlonglong, QString, qlonglong, QStringList)
    arguments << QVariant(m_authenticationHash);
    arguments << path;
    arguments << (qlonglong)0L;
    arguments << name;
    arguments << "";
    arguments << "SBML";
    arguments << "SBML";
    arguments << "JummpLoader";
    arguments << (qlonglong)0;
    arguments << "";
    arguments << (qlonglong)0;
    arguments << QStringList();
    QDBusReply<qlonglong> reply = m_modelInterface->callWithArgumentList(QDBus::Block, "uploadModel", arguments);
    if (!reply.isValid()) {
        m_errors.insert(name, reply.error().message());
    } else if (m_publish) {
        m_modelInterface->call("publishModelRevision", m_authenticationHash, reply.value(), 1);
    }
}

void JummpLoader::printResults()
{
    std::cout << std::endl;
    if (m_errors.isEmpty()) {
        std::cout << "All Model Files imported successfully" << std::endl;
        return;
    }
    std::cout << "Not all Models could be imported. The following files failed with the given reason:" << std::endl;
    std::cout << "===================================================================================" << std::endl;
    QMap<QString, QString>::const_iterator it = m_errors.constBegin();
    while (it != m_errors.constEnd()) {
        std::cout << it.key().toUtf8().constData() << ":" << it.value().toUtf8().constData() << std::endl;
        ++it;
    }
}

void JummpLoader::performImport()
{
    const QFileInfoList fileInfoList = m_dir.entryInfoList(QStringList("*.xml"));
    foreach (const QFileInfo &info, fileInfoList) {
        if (!info.isFile() || !info.isReadable()) {
            continue;
        }
        performFileImport(info);
    }
}

void JummpLoader::performFileImport(const QFileInfo& info)
{
    QFile file(info.filePath());
    QString name;
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        name = extractName(&file);
    }
    if (name.isEmpty()) {
        name = info.baseName();
    }
    file.close();
    importModel(info.absoluteFilePath(), name);
}

void JummpLoader::feedback()
{
    std::cout << ".";
    std::cout.flush();
}

void JummpLoader::handleProcessingFinished()
{
    m_feedbackTimer->stop();
    printResults();
    emit quit();
}

QString JummpLoader::extractName(QFile *file)
{
    m_xml.setDevice(file);
    if (!m_xml.readNextStartElement()) {
        return QString();
    }

    if (m_xml.name() != "sbml") {
        return QString();
    }
    while (!m_xml.atEnd()) {
        if (m_xml.readNext() != QXmlStreamReader::StartElement) {
            continue;
        }
        if (m_xml.name() == "model") {
            if (m_xml.attributes().hasAttribute("name")) {
                return m_xml.attributes().value("name").toString();
            }
            if (m_xml.attributes().hasAttribute("id")) {
                return m_xml.attributes().value("id").toString();
            }
            return QString();
        }
    }

    return QString();
}

#include "jummp-loader.moc"
